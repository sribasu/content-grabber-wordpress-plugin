# Content Grabber - Wordpress Plugin for Scraping Contents from Webpage

Content Grabber grabs content of a URL (user input) and presents it with simple Post Edit options and allows user to save it as a Blog Post instantly.

## Grabbing contents from a webpage

You will see a section below with title **Grab Webpage Content**. This section allows you to enter the URL of a webpage. This webpage is then crawled by the plugin and raw contents are downloaded and displayed on the next section called **Save Grabbed Contents**. Optionally while entering the URL you can specify one or more DOM Selector of the webpage, open selector per line in the designated input field. The plugin uses a PHP Library called _Simple DOM Parser_. You can see the supported DOM selectors [here](http://simplehtmldom.sourceforge.net/manual.htm). When DOM selectors are provided, the Webpage is first downloaded and only matching DOM elements are extracted from the page and displayed in the Save Grabbed Contents form. If multiple DOM selectors are given all the matching sections of the webpage are concatenated and presented as the final Grabbed content.

## Example:

If you want to extract the contents inside `article` tag of a URL http://pagetograbcontentfrom.com enter as following:

`URL: http://pagetograbcontentfrom.com  
DOM Selectors:  
article`

If you want to extract the contents from `div` tags with ids 'content-main', 'content-summary' of a URL http://pagetograbcontentfrom.com enter as following:

`URL: http://pagetograbcontentfrom.com  
DOM Selectors:  
div[id=content-main]  
div[id=content-summary]`

Optionally multiple selectors can be specified in a single line with CSS like multi-selectors:

`URL: http://pagetograbcontentfrom.com  
DOM Selectors:  
div[class=header]  
div[id=main_content], p[id=footer]`

## Saving the grabbed contents

The plugin provides a minimal interface to publish or save Drafts of Blog Posts. Three important fields are present - Post Title, Post URL Slug and Post Content/Body. These fields are enough to save a basic blog post. The blog Post is saved against the Wordpress user who is logged in and is also assigned the default Post Category as confired in Wordpress Settings. If you want to change any other attributes of the Post, you have to save the Blog Post as a Draft and Continue editing the post in the Default Wordpress Post Editor. There are 3 buttons at the end of **Save Grabbed Contents** section. _Save Draft & Edit in Post Editor_ saves the grabbed contents as a draft post and takes you to the Wordpress Standard Post Editor to further modify other attributes of the Post. _Save Draft & Grab New URL_ saves the grabbed contents as draft post and resets the Content Grabber plugin page. _Publish Post & Grab New URL_ button published the grabbed contents as a Blog Post directly from this editor and the Post instantly becomes available to your readers.

## Notes:

1.  By default `script`, `link`, `meta` and `style` tags are removed from the contents of the webpage. If you want to allow them, select the designated checkboxes in the form below. Please note, allowing script and style tags might introduce melicious code and unwanted look and feel changes on your own blog pages. So, please do not allow them unless you are sure.
2.  When no DOM selector is specified, the default selector becomes `body`. Hence only body contents of the webpage is extracted and presented on the next form to save Blog Post.
3.  Raw contents of the URL is downloded on the server where this Wordpress Blog is hosted. If the URL contains any melicious contents, the server might get infected. Make sure you use a safe webpage to extract contents from.
4.  For non HTML URLs, the plugin won't work properly. It would fail to extract content from a page which is not in HTML.

## Disclaimer / Legal Notice (Important):

Content Grabber is a Scraper tool. Scraper tools are often used for intensional copyright infringements. When you are free to decide how you want to use the tool, the Content Grabber tool or its developers doesn't own any responsibility of any breaches and infringements done with the tool. As a developer, we request the users to not use it for any unlawful purposes and to not breach law while using it. Copyrights must not be breached at any time. You will get penalized by the law/search engines either materially or by other means if you steal copyright protected contents from others websites.


## Credits

Content Grabber is developed by [JujuApps Technologies](http://jujuapps.com "JujuApps Technologies - Mobile app and website developer"). Visit [Blog of Prithwiraj Bose](http://sribasu.com "Blog of Prithwiraj Bose") to learn Java and PHP Programming.