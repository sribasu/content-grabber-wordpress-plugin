<?php
/**
* Plugin Name: Content Grabber
* Plugin URI: http://jujuapps.com/content-grabber
* Description: Content Grabber grabs contents from a web page and publishes it as a Blog Post in your Blog.
* Version: 1.0
* Author: JujuApps Technologies
* Author URI: http://jujuapps.com
* License: GPL12
*/
require_once('rss-grabber.php');
require_once('helper-functions.php');

function addContentGrabber() {
	add_menu_page( 'Content Grabber', 'Content Grabber', 'manage_options', 'content-grabber', 'viewContentGrabber', 	'dashicons-welcome-add-page', 6  );
	add_submenu_page( 'content-grabber', 'RSS Grabber', 'RSS Grabber', 'manage_options', 'rss-grabber', 'viewRssGrabber');
}

function register_content_grabber_session(){
    if( !session_id() )
        session_start();

	if(!isset($_SESSION['content_grabber_technique']) || empty($_SESSION['content_grabber_technique'])) {
		$_SESSION['content_grabber_technique']="curl";
	}

	if(!isset($_SESSION['content_grabber_proxy'])) {
		$_SESSION['content_grabber_proxy']="";
	}
}

function viewContentGrabber() {

	if(isset($_REQUEST['content_grabber_action']) && !empty($_REQUEST['content_grabber_action'])) {
		$postarr = array(
			'post_content'=> $_REQUEST['grabbed_content_editor'],
			'post_title'=> $_REQUEST['content_title'],
			'post_name'=> $_REQUEST['content_url'],
			'post_status'=> ($_REQUEST['content_grabber_action']=='publish_content' ? 'publish' : 'draft'),
		);
		$postarr = sanitize_post($postarr, 'db');
		$insertid = wp_insert_post($postarr, false);
		if($insertid>0) {
			echo '<h1 id="content_grabber_status">Success</h1><CONTENT_GRABBER_POST_ID>'.$insertid.'</CONTENT_GRABBER_POST_ID>';
		}
		else {
			echo '<h1 id="content_grabber_status">Failure</h1>';
		}
	}
	else {
		
		if(isset($_REQUEST['content_grabber_technique'])) {
			$_SESSION['content_grabber_technique'] = $_REQUEST['content_grabber_technique'];
		}

		if(isset($_REQUEST['content_grabber_proxy'])) {
			$_SESSION['content_grabber_proxy'] = $_REQUEST['content_grabber_proxy'];
		}
		?>
		<style type="text/css">
			.grabberwrap {
				padding: 7px 30px 10px 30px;
				background: #fff;
				border: 1px solid #999;
				border-radius: 10px;
			}

			.menucontainer {
				margin-bottom:30px;
			}

			.menuitem {
				font-size:24px;
				font-weight:bold;
				color: #111;
				text-decoration: none;
				margin-right: 5px;
				background: #ddd;
				border: 1px solid #999;
				padding: 0 20px 10px 20px;
				border-radius: 0 0 10px 10px;
			}

			.menuitem.active {
				color: #fff;
				background: #333;
				border: 1px solid #111;
			}
		</style>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#showinfo_contentgrabber, #close_content_grabber_help_information_container').click(function(e) {
					$('#content_grabber_help_information').toggle(500);

				});
			});
			function validateContentGrabberForm() {
				if($.trim($('#content_grabber_url').val())=='') {
					alert("Please Enter the URL to grab the contents from.");
					return false;
				}
				return true;
			}
		</script>
		<div class="wrap grabberwrap"><div class="menucontainer">
			<a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=content-grabber" class="menuitem active">Content Grabber</a>
            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?page=rss-grabber" class="menuitem">RSS Grabber</a>
		</div>
		<h3>Grab contents from an external webpage and publish them as Blog Post on your Blog.</h3>
		<p>Before you start using the tool, <a href="#" id="showinfo_contentgrabber">Read this</a> to know how it works.</p>

		<div id="content_grabber_help_information" style="display:none; width: 90%; padding: 10px 30px; border: 1px solid #ddd; background: #ffedc6; border-radius: 8px; margin:10px 0 30px 0">
			<div style="float:right; clear: both;" id="close_content_grabber_help_information_container">
			<a href="#" id="close_content_grabber_help_information">close</a></div>
			<h2 style="float:left; clear:right; display:inline;">Help</h2>
			<div style="clear:both">&nbsp;</div>
			<p>Content Grabber grabs content of a URL (user input) and presents it with simple Post
			Edit options and allows user to save it as a Blog Post instantly.</p>
			<h3>Grabbing contents from a webpage</h3>
			<p>You will see a section below with title <strong>Grab Webpage Content</strong>. This section allows you to
			enter the URL of a webpage. This webpage is then crawled by the plugin and raw contents are downloaded and displayed
			on the next section called <strong>Save Grabbed Contents</strong>. Optionally while entering the URL you can specify
			one or more DOM Selector of the webpage, open selector per line in the designated input field. The plugin uses a PHP Library
			called <i>Simple DOM Parser</i>. You can see the
			supported DOM selectors <a href="http://simplehtmldom.sourceforge.net/manual.htm" target="_blank">here</a>. When
			DOM selectors are provided, the Webpage is first downloaded and only matching DOM elements are extracted from
			the page and displayed in the Save Grabbed Contents form. If multiple DOM selectors are given all the matching
			sections of the webpage are concatenated and presented as the final Grabbed content.</p>
			<h4>Example:</h4>
			<p>If you want to extract the contents inside <code>article</code> tag of a URL http://pagetograbcontentfrom.com enter as
			following:</p>
			<p><code>URL: http://pagetograbcontentfrom.com<br />
			DOM Selectors:<br />
			article</code></p>
			<p>If you want to extract the contents from <code>div</code> tags with ids 'content-main', 'content-summary' of a URL http://pagetograbcontentfrom.com enter as
			following:</p>
			<p><code>URL: http://pagetograbcontentfrom.com<br />
			DOM Selectors:<br />
			div[id=content-main]<br />
			div[id=content-summary]</code></p>
			<p>Optionally multiple selectors can be specified in a single line with CSS like multi-selectors:</p>
			<p><code>URL: http://pagetograbcontentfrom.com<br />
			DOM Selectors:<br />
			div[class=header]<br />
			div[id=main_content], p[id=footer]</code></p>

			<h3>Saving the grabbed contents</h3>
			<p>The plugin provides a minimal interface to publish or save Drafts of Blog Posts. Three important
			fields are present - Post Title, Post URL Slug and Post Content/Body. These fields are enough to save
			a basic blog post. The blog Post is saved against the Wordpress user who is logged in and is also
			assigned the default Post Category as confired in Wordpress Settings. If you want to change any other
			attributes of the Post, you have to save the Blog Post as a Draft and Continue editing the post in
			the Default Wordpress Post Editor. There are 3 buttons at the end of <strong>Save Grabbed Contents</strong>
			section. <i>Save Draft &amp; Edit in Post Editor</i> saves the grabbed contents as a draft post and takes
			you to the Wordpress Standard Post Editor to further modify other attributes of the Post.
			<i>Save Draft &amp; Grab New URL</i> saves the grabbed contents as draft post and resets the Content Grabber
			plugin page. <i>Publish Post &amp; Grab New URL</i> button published the grabbed contents as a Blog Post
			directly from this editor and the Post instantly becomes available to your readers.
			</p>
			<h3>Notes:</h3>
			<ol><li>By default <code>script</code>, <code>link</code>, <code>meta</code> and <code>style</code> tags
			are removed from the contents of the webpage. If you want to allow them, select the designated checkboxes
			in the form below. Please note, allowing script and style tags might introduce melicious code and unwanted
			look and feel changes on your own blog pages. So, please do not allow them unless you are sure.</li>
			<li>When no DOM selector is specified, the default selector becomes <code>body</code>. Hence only body contents
			of the webpage is extracted and presented on the next form to save Blog Post.</li>
			<li>Raw contents of the URL is downloded on the server where this Wordpress Blog is hosted. If the URL contains
			any melicious contents, the server might get infected. Make sure you use a safe webpage to extract contents
			from.</li>
			<li>For non HTML URLs, the plugin won't work properly. It would fail to extract content from a page which is
			not in HTML.</li>
			</ol>
			<h3>Disclaimer / Legal Notice (Important):</h3>
			<p>Content Grabber is a Scraper tool. Scraper tools are often used for intensional copyright infringements.
			When you are free to decide how you want to use the tool, the Content Grabber tool or its developers doesn't
			own any responsibility of any breaches and infringements done with the tool. As a developer, we request the users
			to not use it for any unlawful purposes and to not breach law while using it. Copyrights must not be breached at
			any time. You will get penalized by the law/search engines either materially or by other means
			if you steal copyright protected contents from others websites.</p>
		</div>

		
		<div class="content-grabber">
		<div id="grabber_main_form_container" style="width:90%; border: 1px solid #333; background:#ebffe5; padding: 20px 30px;">
		<h3>Grab Webpage Content</h3>
		<form method="post" onsubmit="return validateContentGrabberForm();">
		<p>Enter URL of the Webpage to grab contents from:<br />
		<input type="text" name="content_grabber_url" id="content_grabber_url" style="width:80%; font-family: monospace;" value="<?php if(!empty($_REQUEST['content_grabber_url'])) echo $_REQUEST['content_grabber_url']; ?>" /></p>
		<p>DOM Selectors for Main Contents One per line (Optional, default is <code>body</code>):<br />
		<textarea name="content_grabber_selectors" id="content_grabber_selectors" style="width:80%; height: 100px; font-family: monospace;"><?php if(!empty($_REQUEST['content_grabber_selectors'])) echo $_REQUEST['content_grabber_selectors']; ?></textarea></p>
		<p>Content Cleanup Options<br />
		<span style="margin-right:20px;"><input type="checkbox" name="content_grabber_script_opt" id="content_grabber_script_opt" 
		value="y"<?php if(isset($_REQUEST['content_grabber_script_opt']) 
		 && $_REQUEST['content_grabber_script_opt']=='y') echo ' checked="true"'; ?> />&nbsp;Allow Scripts</span>
		
		<span style="margin-right:20px;"><input type="checkbox" name="content_grabber_style_opt" id="content_grabber_style_opt" 
		value="y"<?php if(isset($_REQUEST['content_grabber_style_opt']) 
		 && $_REQUEST['content_grabber_style_opt']=='y') echo ' checked="true"'; ?> />&nbsp;Allow Styles</span>
		
		<span style="margin-right:20px;"><input type="checkbox" name="content_grabber_tags_opt" id="content_grabber_tags_opt" 
		value="y"<?php if(isset($_REQUEST['content_grabber_tags_opt']) 
		 && $_REQUEST['content_grabber_tags_opt']=='y') echo ' checked="true"'; ?> />&nbsp;Allow Other Unsafe Tags</span>
		</p>
		<p>Remote Server Connection Settings (leave Defaults if not sure)<br />
		<span style="margin-right:20px;">Connection Using&nbsp;
			<select name="content_grabber_technique" id="content_grabber_technique">
				<option value="curl"<?php if(!isset($_SESSION['content_grabber_technique']) 
					|| $_SESSION['content_grabber_technique']!='filegetcontents') echo ' selected="selected"'; ?>>cURL</option>
				<option value="filegetcontents"<?php if(isset($_SESSION['content_grabber_technique']) 
					&& $_SESSION['content_grabber_technique']=='filegetcontents') echo ' selected="selected"'; ?>>file_get_contents()</option>
			</select>
		</span>
		<span style="margin-right:20px;">Proxy Server URL (if applicable)&nbsp;
		<input type="text" name="content_grabber_proxy" id="content_grabber_proxy" style="font-family: monospace;" 
			value="<?php if(!empty($_SESSION['content_grabber_proxy'])) echo $_SESSION['content_grabber_proxy']; ?>" /></p>
		</span>
		</p>
		<p><input type="submit" name="content_grabber_submit" value="Grab Contents" /></p>
		</form>
		</div>
		<?php
		if(isset($_REQUEST['content_grabber_submit']) && !empty($_REQUEST['content_grabber_url'])) {
			require_once('simple_html_dom.php');
			$htmlContent = "";
			if(!isset($_SESSION['content_grabber_technique']) || $_SESSION['content_grabber_technique']!='filegetcontents') {
				$proxy = ((isset($_SESSION['content_grabber_proxy']) && trim($_SESSION['content_grabber_proxy'])!='') ? $_SESSION['content_grabber_proxy'] : '');

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $_REQUEST['content_grabber_url']);
				if($proxy!='') {
					curl_setopt($ch, CURLOPT_PROXY, $proxy);
				}
				curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookies.txt');
				curl_setopt($ch, CURLOPT_COOKIEFILE, 'cookies.txt');
				curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HEADER, 1);
				$htmlContent = curl_exec($ch);
				curl_close($ch);
			}
			else {
				$opts = [
					"http" => [
						"method" => "GET",
						"header" => "Accept-language: en\r\n" .
							"User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13\r\n"
					]
				];

				if(isset($_SESSION['content_grabber_proxy']) && trim($_SESSION['content_grabber_proxy'])!='') {
					$opts['http']['proxy'] = $_SESSION['content_grabber_proxy'];
				}
				$context = stream_context_create($opts);
				$htmlContent = file_get_contents($_REQUEST['content_grabber_url'], false, $context);
			}

			if($htmlContent==NULL || empty($htmlContent) || trim($htmlContent)=='') {
				print '<hr /><h2 style="color:red">Failed to fetch contents from the URL provided</h2>'.
					'<p>Please make sure that your connection options are correct and are applicable on your server. If you still get this error, it means the site (of the given URL) uses smart techniques to detect non-browser/non-human requests and prevent them from accessing it.</p>';
			}
			else {

				$html = str_get_html($htmlContent);

				$finalcontent = '';

				$disallowedtags = array();
				if(!isset($_REQUEST['content_grabber_script_opt']) || $_REQUEST['content_grabber_script_opt']!='y') {
					array_push($disallowedtags,"script");
					array_push($disallowedtags,"meta");
				}
				if(!isset($_REQUEST['content_grabber_style_opt']) || $_REQUEST['content_grabber_style_opt']!='y') {
					array_push($disallowedtags,"style");
					array_push($disallowedtags,"link");
				}

				if(sizeof($disallowedtags)>0) {
					$remtags = $html->find(implode(",",$disallowedtags));
					foreach($remtags as $tag) {
						$tag->outertext='';
					}
					$html->load($html->save());
				}
				
				$selectorTxt = 'body';
				if(!empty($_REQUEST['content_grabber_selectors'])) {
					$selectorTxt = trim($_REQUEST['content_grabber_selectors']);
				}

				$selectors = explode('\n',str_replace("\r", "", $selectorTxt));
				foreach($selectors as $selector) {
					if(!empty(trim($selector))) {
						$domobj = $html->find(trim($selector));
						if($domobj!=NULL && is_array($domobj) && sizeof($domobj)>0) {
							for($i=0; $i<sizeof($domobj); $i++) {
								$finalcontent .= $domobj[$i]->innertext;
							}
						}
					}
				}

				if(isset($_REQUEST['content_grabber_tags_opt']) && $_REQUEST['content_grabber_tags_opt']=='y') {
					$allowedtags = '<a>,<img>,<div>,<p>,<b>,<strong>,<article>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<pre>,<code>,<i>,<br>,<hr>,<ul>,<ol>,<li>,<span>,<table>,<tbody>,<thead>,<th>,<tr>,<td>';
					if(isset($_REQUEST['content_grabber_script_opt']) && $_REQUEST['content_grabber_script_opt']=='y') {
						$allowedtags .= ',<meta>,<script>';
					}
					if(isset($_REQUEST['content_grabber_style_opt']) && $_REQUEST['content_grabber_style_opt']=='y') {
						$allowedtags .= ',<link>,<style>';
					}
					$finalcontent = strip_tags($finalcontent,$allowedtags);
				}
				$pagetitle = $html->find('head title',0)->innertext;

				print '<hr /><div id="grabbed_content_container" style="width:90%; min-height: 200px; '.
					'border: 1px solid #333; background:#dbeeff; padding: 20px 30px;"><form method="post" '.
					'name="same_grabbed_content_form" id="same_grabbed_content_form"><h1>Save Grabbed Contents</h1>';
				print '<p>Post Title<br /><input type="text" name="content_title" id="content_title" value="' . $pagetitle .
				'" style="width:80%; font-size:18px; font-family: monospace;" /></p><p>Post URL Slug<br />'.
				'<input type="text" name="content_url" id="content_url" value="' . sanitize_title($pagetitle) .
				'" style="width:80%; font-size:18px; font-family: monospace;" /></p><p>Post Content</br />';
				wp_editor($finalcontent,'grabbed_content_editor'); ?>
				</p><p style="text-align:right; width: 100%;"><input type="hidden" id="content_grabber_action"
				name="content_grabber_action" value="edit" /><input type="button" id="edit_draft" 
				value="Save Draft &amp; Edit in Post Editor" />&nbsp;<input type="button" id="save_draft"
				value="Save Draft &amp; Grab New URL" />&nbsp;<input type="button" id="publish_content" 
				value="Publish Post &amp; Grab New URL" /></p></form></div>
				
				<script type="text/javascript">
				$(document).ready(function() {
					$('#edit_draft, #publish_content, #save_draft').click(function(e) {
						if($.trim($('#content_title').val())=='' || $.trim($('#content_url').val())=='') {
							alert("Please Enter the Post Title, URL and Content");
							return false;
						}
						var thisid = $(this).attr('id');
						$('#content_grabber_action').val(thisid);
						$('#edit_draft, #publish_content, #save_draft').prop('disabled',true);
						if(tinyMCE) {
							$('#grabbed_content_editor').val(tinyMCE.activeEditor.getContent());
						}
						$.ajax({
							url: window.location.href,
							method: 'post',
							data: $('#same_grabbed_content_form').serialize(),
							dataType: 'html',
							success: function(resp) {
								if(resp.indexOf('<h1 id="content_grabber_status">Success</h1>')!==-1) {
									try {
										var insertid = resp.substring(resp.indexOf('<CONTENT_GRABBER_POST_ID>')+25, resp.indexOf('</CONTENT_GRABBER_POST_ID>'));
										if(thisid=='edit_draft') {
											window.location = 'post.php?post='+$.trim(insertid)+'&action=edit';
										}
										else {
											document.same_grabbed_content_form.reset();
											alert("Post "+ (thisid=='publish_content' ? 'published' : 'saved') +" successfully");
											location.reload(true);
										}
									}
									catch(e) {
										$('#edit_draft, #publish_content, #save_draft').prop('disabled',false);
										alert("Failed to recognize the response from server! Post not saved.");
									}
								}
								else {
									$('#edit_draft, #publish_content, #save_draft').prop('disabled',false);
									alert("Failed to save draft of the post");
								}
							},
							error: function(xhr) {
								$('#edit_draft, #publish_content, #save_draft').prop('disabled',false);
								alert("An error has occured while saving draft of the post");
							}
						});
					});
				});
				</script>
				<?php

			}

		}
		?>

		<div class="footer_credit_grabber"
		style="margin-top:30px; margin-bottom:30px; width 90%; text-align:center; letter-spacing:2px;">
		This plugin is developed by <a href="http://jujuapps.com" target="_blank"
		title="JujuApps.com - opens in new tab">JujuApps Technologies</a></div>
		</div>
		</div>
		<?php
    }
}

register_activation_hook( __FILE__, 'rss_grabber_create_table' );
add_action('init','register_content_grabber_session');
add_action( 'admin_menu', 'addContentGrabber' );
?>