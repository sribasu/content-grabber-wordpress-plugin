<?php

global $rss_grabber_version;
$rss_grabber_version = '1.0';

function rss_grabber_create_table() {
	global $wpdb;
	global $rss_grabber_version;
	$table_name = $wpdb->prefix . 'rss_grabber';
	$post_tablename = $wpdb->prefix . 'posts';
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		grabbed_page_id mediumint(9) NOT NULL AUTO_INCREMENT,
		create_ts datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
		grabbed_page_url varchar(150) NOT NULL,
	    blog_post_id mediumint(9) NOT NULL,
		PRIMARY KEY  (grabbed_page_id)
	) $charset_collate;";
	 

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	
	add_option( 'rss_grabber_version', $rss_grabber_version );
    
    /*$installed_ver = get_option( "rss_grabber_version" );

    if ( $installed_ver != $jal_db_version ) {

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            grabbed_page_id mediumint(9) NOT NULL AUTO_INCREMENT,
            time create_ts DEFAULT CURRENT_TIMESTAMP NOT NULL,
            grabbed_page_url varchar(150) NOT NULL,
            blog_post_id mediumint(9) DEFAULT 0 NOT NULL,
            PRIMARY KEY  (grabbed_page_id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        update_option( "rss_grabber_version", $rss_grabber_version );
    }*/
}

function rss_grabber_load_data() {
	global $wpdb;
	
	$welcome_name = 'Mr. WordPress';
	$welcome_text = 'Congratulations, you just completed the installation!';
	
	$table_name = $wpdb->prefix . 'liveshoutbox';
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'time' => current_time( 'mysql' ), 
			'name' => $welcome_name, 
			'text' => $welcome_text, 
		) 
	);
}

/*function rss_grabber_update_db_check() {
    global $rss_grabber_version;
    $rss_grabber_version=2.0;
    if ( get_site_option( 'rss_grabber_version' ) != $rss_grabber_version ) {
        rss_grabber_create_table();
    }
}
add_action( 'plugins_loaded', 'rss_grabber_update_db_check' );*/
?>